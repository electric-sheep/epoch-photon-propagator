#include <iostream>
#include <cmath>
#include <experimental/filesystem>
#include <vector>
#include <memory>
#include <tuple>

#include <boost/multi_array.hpp>

#include "H5Cpp.h"

#include "common.h"
#include "sdf_file.h"


using namespace H5;

using namespace std;
using namespace experimental::filesystem;

using boost::multi_array, boost::extents;

path get_base_file(int argc, char** argv){
    if(argc != 2) fatal("Wrong arguments! Specify SDF file!");
    path full_path = path(argv[1]);
    if (full_path.extension() != ".sdf") fatal("Not a '.sdf' file!");
    path parent = full_path.parent_path();
    path stem = full_path.stem();
    return parent/stem;
}


int main(int argc, char** argv){

    path base_name = get_base_file(argc, argv);

    auto sdf_file = make_unique<SDFFile>(path(base_name.string() + string(".sdf")));
    auto output_filename = path(base_name.string() + string(".h5")).string();

    double time =  sdf_file->time();
    long npart = sdf_file->npart();
    cout << "t = " << time << ", npart = " << npart << endl;

    auto x0 = sdf_file->data<double>("initial x/subset_phot/photon");
    auto y0 = sdf_file->data<double>("initial y/subset_phot/photon");
    auto t0 = sdf_file->data<double>("creation time/subset_phot/photon");
    auto px = sdf_file->data<double>("px/subset_phot/photon");
    auto py = sdf_file->data<double>("py/subset_phot/photon");
    auto pz = sdf_file->data<double>("pz/subset_phot/photon");
    auto ek = sdf_file->data<double>("ek/subset_phot/photon");
    auto weight = sdf_file->data<double>("weight/subset_phot/photon");
    auto tag = sdf_file->data<int>("tag/subset_phot/photon");

    // Propagate particles, removing those outside bounds

    vector<double> x(npart), y(npart);

    for(int n = 0; n < npart; ++n){
        double p = sqrt ( sqr(px[n]) + sqr(py[n]) + sqr(pz[n]) );
        x[n] = x0[n] + c*t0[n]*px[n]/p;
        y[n] = y0[n] + c*t0[n]*py[n]/p;
    }

    H5File* output_file = new H5File( output_filename, H5F_ACC_TRUNC );
    hsize_t npart_total[1] = { static_cast<hsize_t>(npart) };
    DataSpace fspace(1, npart_total);
    DataSet* ds_x = new DataSet(output_file->createDataSet(
        "x", PredType::NATIVE_DOUBLE, fspace));
    DataSet* ds_y = new DataSet(output_file->createDataSet(
        "y", PredType::NATIVE_DOUBLE, fspace));
    DataSet* ds_px = new DataSet(output_file->createDataSet(
        "Px", PredType::NATIVE_DOUBLE, fspace));
    DataSet* ds_py = new DataSet(output_file->createDataSet(
        "Py", PredType::NATIVE_DOUBLE, fspace));
    DataSet* ds_pz = new DataSet(output_file->createDataSet(
        "Pz", PredType::NATIVE_DOUBLE, fspace));
    DataSet* ds_ek = new DataSet(output_file->createDataSet(
        "Ek", PredType::NATIVE_DOUBLE, fspace));
    DataSet* ds_weight = new DataSet(output_file->createDataSet(
        "Weight", PredType::NATIVE_DOUBLE, fspace));
    DataSet* ds_tag = new DataSet(output_file->createDataSet(
        "Tag", PredType::NATIVE_INT, fspace));

    ds_x->write(x.data(), PredType::NATIVE_DOUBLE);
    ds_y->write(y.data(), PredType::NATIVE_DOUBLE);
    ds_px->write(px, PredType::NATIVE_DOUBLE);
    ds_py->write(py, PredType::NATIVE_DOUBLE);
    ds_pz->write(pz, PredType::NATIVE_DOUBLE);
    ds_ek->write(ek, PredType::NATIVE_DOUBLE);
    ds_weight->write(weight, PredType::NATIVE_DOUBLE);
    ds_tag->write(tag, PredType::NATIVE_INT);

    path parent = base_name.parent_path();
    auto ffile = SDFFile(parent / "f0000.sdf");
    BoxSize box = ffile.box_size();

    // Prepare the density array
    multi_array<float, 2> density_rr(extents[box.nx][box.ny]);
    multi_array<float, 2> density_bs(extents[box.nx][box.ny]);
    for (int i=0; i<box.nx; ++i) {
        for (int j=0; j<box.ny; ++j) {
            density_rr[i][j] = 0;
            density_bs[i][j] = 0;
        }
    }
    double dx = (box.xmax - box.xmin) / static_cast<double>(box.nx);
    double dy = (box.ymax - box.ymin) / static_cast<double>(box.ny);

    // Integrate density
    for (int n=0; n < npart; ++n){
        if(tag[n] == 1){
            int i = static_cast<int>(floor(x[n]/dx - box.xmin));
            int j = static_cast<int>(floor(y[n]/dy - box.ymin));
            if (i>=0 && i<box.nx && j>=0 && j<box.ny){
                if(tag[n] == 1){
                    density_rr[i][j] += static_cast<float>(weight[n]);
                }else{
                    density_bs[i][j] += static_cast<float>(weight[n]);
                }
            }
        }
    }

    hsize_t fdim[] = {static_cast<unsigned>(box.nx), static_cast<unsigned>(box.ny)};
    DataSpace nfspace(2, fdim);
    DataSet* ds_rr = new DataSet(output_file->createDataSet(
        "density_rr", PredType::NATIVE_FLOAT, nfspace));
    DataSet* ds_bs = new DataSet(output_file->createDataSet(
        "density_bs", PredType::NATIVE_FLOAT, nfspace));
    ds_rr->write(density_rr.data(), PredType::NATIVE_FLOAT);
    ds_bs->write(density_bs.data(), PredType::NATIVE_FLOAT);

    return 0;
}
