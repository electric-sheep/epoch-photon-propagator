/*
 * sdf_file.h
 *
 *  Created on: May 30, 2018
 *      Author: strazce
 */

#ifndef SRC_SDF_FILE_H_
#define SRC_SDF_FILE_H_

#include <experimental/filesystem>
#include <memory>

#include <sdf.h>
#include <sdf_helper.h>
#include "common.h"

struct BoxSize {
    int nx, ny;
    double xmin, xmax;
    double ymin, ymax;
};

class SDFFile {
private:
    std::unique_ptr<sdf_file_t> _file_handle;
public:
    SDFFile(std::experimental::filesystem::path filename)
        : _file_handle(std::make_unique<sdf_file_t>(
                *sdf_open(filename.c_str(), MPI_COMM_WORLD, SDF_READ, 0))
                )
        {
        if(!sdf_read_header(_file_handle.get())) fatal("Error reading SDF header");
        sdf_read_blocklist_all(_file_handle.get());
    }

    ~SDFFile(){
        sdf_close(_file_handle.get());
    }

    template<typename T>
    T* data(const char *block_id){
        auto block = std::make_unique<sdf_block>(
                *sdf_find_block_by_id(_file_handle.get(), block_id)
                );
        _file_handle->current_block = block.get();
        sdf_read_data(_file_handle.get());
        return static_cast<T*>(block->data);
    }

    /// Retrieves the number of photons form the Px variable
    long npart(){
        auto px_block = std::make_unique<sdf_block>(
                *sdf_find_block_by_id(_file_handle.get(), "px/subset_phot/photon")
                );
        return px_block->dims[0];
    }

    /// Retrieves the box size from the grid
    BoxSize box_size(){
        auto grid = std::make_unique<sdf_block>(
                *sdf_find_block_by_id(_file_handle.get(), "grid")
                );
        BoxSize box;
        box.nx = grid->dims[0];
        box.ny = grid->dims[1];
        box.xmin = grid->extents[0];
        box.ymin = grid->extents[1];
        box.xmax = grid->extents[2];
        box.ymax = grid->extents[3];
        return box;
    }

    double time(){
        return _file_handle->time;
    }
};


#endif /* SRC_SDF_FILE_H_ */
