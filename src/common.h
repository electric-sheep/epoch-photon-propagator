/*
 * common.h
 *
 *  Created on: May 30, 2018
 *      Author: strazce
 */

#ifndef SRC_COMMON_H_
#define SRC_COMMON_H_

#include <iostream>
#include <cstdlib>
#include <string>

static const int MPI_COMM_WORLD = 0;
static const double c = 299792458;

template<typename T>
inline static constexpr T sqr(T a) {
    return a*a;
}

void fatal(std::string message, int code = 1){
    std::cout << "ERROR: " << message  << std::endl;
    std::exit(code);
}



#endif /* SRC_COMMON_H_ */
