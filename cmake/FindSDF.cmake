# find SDF library

find_library(SDF_C_LIBRARIES NAMES libsdfc.a sdf PATHS ${PROJECT_BINARY_DIR}/lib HINTS $ENV{SDF_LIB})
if(SDF_C_LIBRARIES)
  find_path(SDF_C_INCLUDE_PATH NAMES sdf.h PATHS ${PROJECT_BINARY_DIR}/include HINTS $ENV{SDF_INC})
endif(SDF_C_LIBRARIES)

include (FindPackageHandleStandardArgs)
find_package_handle_standard_args(SDF DEFAULT_MSG SDF_C_LIBRARIES SDF_C_INCLUDE_PATH)
